# import warnings
#
# warnings.filterwarnings("ignore")

import time
import sys
import pandas as pd
import json

from pyspark import SQLContext
from pyspark.ml.classification import MultilayerPerceptronClassificationModel
from pyspark.ml.evaluation import MulticlassClassificationEvaluator
from pyspark.ml.feature import Normalizer, StandardScalerModel
from pyspark.sql import SparkSession
from pyspark.ml.linalg import Vectors
from pyspark.sql.functions import rand
from pyspark.ml.feature import StandardScaler
# SparkSession.builder.
from pyspark.sql.types import StructType, StructField

spark = SparkSession \
    .builder \
    .appName("myApp") \
    .getOrCreate()



sc = spark.sparkContext
sqlContext = SQLContext(sc)
# df = spark.read.format("com.mongodb.spark.sql.DefaultSource").load()
#########################################
'''REAL TIME PACHETS'''
input = sys.argv[1]
df_pd = pd.read_json(input)
#########################################

##########################################
# '''DEBUG LOCAL'''
# input = 'prova.txt'
# df_pd = pd.read_json(input)
##########################################

SUB_NET_PREFIX = '[10, 0, 0,'

#df_pd = pd.read_json('coso.txt')

def estraiFeatures(x):
    daddr = x['daddr']['addr']
    saddr = x['saddr']['addr']
    sec = x['sec']
    usec = x['usec']
    datalength = x['dataLength']
    return daddr,saddr,sec,usec,datalength

df = pd.DataFrame((df_pd.apply((estraiFeatures),axis=1).tolist()), columns = ['daddr','saddr', 'sec','usec','datalength'])
df['saddr'] = df['saddr'].astype(str)
df['daddr'] = df['daddr'].astype(str)

df = df.assign(totusec=df['sec']*1000000 + df['usec'])

def flusso_pacchetti (x, df_in):
    send_feats = estrai_pacchetti(df_in, x['saddr'], x['daddr'], x.totusec)
    rec_feats = estrai_pacchetti (df_in, x['daddr'], x['saddr'], x.totusec)
    #t.update(1)
    is_sottorete = 0
    if (x.saddr.startswith(SUB_NET_PREFIX)):
        is_sottorete = 1
    # if (x.saddr[0:3]==SUB_NET_PREFIX):
    #     is_sottorete = 1
    #     print('is sottorete working')

    tot_feats = send_feats+rec_feats
    tot_feats.append(is_sottorete)
    return tot_feats

def estrai_pacchetti (df, s_ip, d_ip, temp):
    df_new = df.loc[(df['saddr'] == s_ip) & (df['daddr'] == d_ip)  & (df['totusec'] < temp)]
    df_new = df_new.loc[(temp - df_new['totusec'] < 30000)]
    return  [df_new.shape[0], sum(df_new['datalength']), df_new['datalength'].mean(), df_new['datalength'].std(), df_new.min()['datalength']]

df = df[['saddr', 'daddr', 'datalength', 'totusec']]
# df_in['saddr'] = df_in['saddr'].astype(str)
# df_in['daddr'] = df_in['daddr'].astype(str)

#t = tqdm(total=df_in.shape[0])

df[['ndatiflusso_s','qdatiflusso_s','mediadatiflusso_s','dsdatiflusso_s', 'mindatiflusso_s',
    'ndatiflusso_r','qdatiflusso_r','mediadatiflusso_r','dsdatiflusso_r', 'mindatiflusso_r',
    'is_sottorete']] = pd.DataFrame(list(df.apply(lambda x: flusso_pacchetti(x, df), axis=1)))

df =df.dropna()
df = df[['datalength',
'dsdatiflusso_r',
'dsdatiflusso_s',
'is_sottorete',
'mediadatiflusso_r',
'mediadatiflusso_s',
'mindatiflusso_r',
'mindatiflusso_s',
'ndatiflusso_r',
'ndatiflusso_s',
'qdatiflusso_r',
'qdatiflusso_s']]


# df = df.apply(lambda x: Vectors.dense(x),axis=1)

df = sqlContext.createDataFrame(df)#,p_schema)

df = (df.rdd.map(lambda x: (x[3],Vectors.dense(x)))).toDF()
df = df.selectExpr("_1 as label", "_2 as features")


model_NN  = MultilayerPerceptronClassificationModel.load("NN_3lab_61")
standardizer = StandardScalerModel.load('standardizer')
df = standardizer.transform(df)

df= df[['features_scaled']]
df = df.selectExpr("features_scaled as features")
predicted_labels=model_NN.transform(df)


output = (predicted_labels.toPandas()).prediction

def most_common(lst):
    return max(set(lst), key=lst.count)

print 'Predicted:', most_common(output.tolist())


import pymongo

data_json = json.loads(pd.DataFrame(output).to_json(orient='records'))

mng_client = pymongo.MongoClient('localhost', 27017)
mng_db = mng_client['VNCC'] # Replace mongo db name
collection_name = 'predizioni' # Replace mongo db collection name
db_cm = mng_db[collection_name]
a=db_cm.insert(data_json)
