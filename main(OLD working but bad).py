# import pymongo
#
# from pymongo import MongoClient
# #client = MongoClient()
# client = MongoClient('localhost', 27017)
# db = client.pippo
# collection = db.banana
#
# import pyspark.mllib

import pyspark.mllib.regression
# SPARK SESSION CONNECTOR
from pyspark.mllib.feature import LabeledPoint

from pyspark.sql import SparkSession
from pyspark.sql import SQLContext
import numpy as np
import pandas as pd

from pyspark import SparkContext

from pyspark.sql.functions import *

#SparkConf().set("spark.jars.packages","org.mongodb.spark:mongo-spark-connector_2.11:2.2.0")

#SparkSession.builder.
spark = SparkSession \
    .builder \
    .appName("myApp") \
    .config("spark.mongodb.input.uri", "mongodb://localhost/VNCC.dataset") \
    .config("spark.mongodb.output.uri", "mongodb://localhost/pippo.banana") \
    .config('spark.jars.packages','org.mongodb.spark:mongo-spark-connector_2.11:2.2.0' ) \
    .getOrCreate()

sc = spark.sparkContext
df = spark.read.format("com.mongodb.spark.sql.DefaultSource").load()

# rdd = df.rdd

'''
./bin/pyspark --conf "spark.mongodb.input.uri=mongodb://127.0.0.1/pippo.banana?readPreference=primaryPreferred" \
              --conf "spark.mongodb.output.uri=mongodb://127.0.0.1/pippo.banana" \
              --packages org.mongodb.spark:mongo-spark-connector_2.11:2.2.2
'''
# xcols = df.columns
# xcols.remove('label')
# xcols.remove('_id')

# training = (rdd.map(lambda x: LabeledPoint(x.label, x.datalength)))
# training.toDF()

df = df.toPandas()

df=df.reindex(np.random.permutation(df.index))
df=df.reset_index(drop=True)

xcols = df.columns.tolist()
xcols.remove('label')
xcols.remove('_id')


from pyspark.ml.linalg import Vectors
def parsePoint(label, feats):
    return (label, Vectors.dense(feats))


# training = df.apply(lambda x: LabeledPoint(x['label'], x[xcols].values), axis=1)
training = df.apply(lambda x: parsePoint(x['label'], x[xcols].values), axis=1)

# TRAINING SESSION
training = training.tolist()

training = spark.createDataFrame(training)

newNames = ['features', 'features']
training = training.selectExpr("_1 as label", "_2 as features")

# LOGISTIC REGRESSION
from pyspark.ml.classification import LogisticRegression

lr = LogisticRegression(maxIter=10, regParam=0.3, elasticNetParam=0.8)


lrModel = lr.fit(training)