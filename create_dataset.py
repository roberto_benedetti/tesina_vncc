import json,sys
import pandas as pd
from tqdm import tqdm
import os

import pymongo

from pymongo import MongoClient
client = MongoClient('localhost', 27017)
db = client.VNCC
collection = db.cattura

data = pd.DataFrame(list(collection.find()))
# collection.remove(collection.find())

'''Current label to be set: 
0 : Internet browsing
1 : Youtube streaming
2 : Spotify streaming 
'''
CURRENT_LABEL = 1

'''Marker for the local subnet'''
SUB_NET_PREFIX = '[10, 0, 0,'

'''Method for adding the label column (Y)'''
def addLabel(i, df):
    return df.assign(label=i)

'''Builds the dataframe from a mongodb session'''
def estraiFeatures(x):
    daddr = x['daddr']['addr']
    saddr = x['saddr']['addr']
    sec = x['sec']
    usec = x['usec']
    datalength = x['dataLength']
    return daddr,saddr,sec,usec,datalength

df = pd.DataFrame((data.payload.apply(estraiFeatures).tolist()), columns = ['daddr','saddr','sec','usec','datalength'])
df['saddr'] = df['saddr'].astype(str)
df['daddr'] = df['daddr'].astype(str)

'''Add the "totusec feature by combining the two time indexes"'''
df = df.assign(totusec=df['sec']*1000000 + df['usec'])
# df['datalength'] = df['datalength'].astype(int)

def flusso_pacchetti (x, df_in):
    send_feats = estrai_pacchetti(df_in, x['saddr'], x['daddr'], x.totusec)
    rec_feats = estrai_pacchetti (df_in, x['daddr'], x['saddr'], x.totusec)
    t.update(1)
    is_sottorete = 0
    if (x.saddr.startswith(SUB_NET_PREFIX)):
        is_sottorete = 1
    # if (x.saddr[0:3]==SUB_NET_PREFIX):
    #     is_sottorete = 1
    #     print('is sottorete working')

    tot_feats = send_feats+rec_feats
    tot_feats.append(is_sottorete)
    return tot_feats

def estrai_pacchetti (df, s_ip, d_ip, temp):
    df_new = df.loc[(df['saddr'] == s_ip) & (df['daddr'] == d_ip)  & (df['totusec'] < temp)]
    df_new = df_new.loc[(temp - df_new['totusec'] < 30000)]
    return  [df_new.shape[0], sum(df_new['datalength']), df_new['datalength'].mean(), df_new['datalength'].std(), df_new.min()['datalength']]

df_in = df[['saddr', 'daddr', 'datalength', 'totusec']]
df_in['saddr'] = df_in['saddr'].astype(str)
df_in['daddr'] = df_in['daddr'].astype(str)



df_vec =[]
BATCH=1000
for i in range(int(df_in.shape[0]/BATCH)):
    df_vec.append(df_in[i*BATCH:i*BATCH+BATCH])


for df_in in df_vec:
    t = tqdm(total=df_in.shape[0])
    data_json=None
    df_fin = None
    df[['ndatiflusso_s','qdatiflusso_s','mediadatiflusso_s','dsdatiflusso_s', 'mindatiflusso_s',
        'ndatiflusso_r','qdatiflusso_r','mediadatiflusso_r','dsdatiflusso_r', 'mindatiflusso_r',
        'is_sottorete']] = pd.DataFrame(list(df_in.apply(lambda x: flusso_pacchetti(x, df_in), axis=1)))

    t.close()

    df_fin = addLabel(CURRENT_LABEL,df[['datalength','ndatiflusso_s','qdatiflusso_s','mediadatiflusso_s','dsdatiflusso_s', 'mindatiflusso_s',
                 'ndatiflusso_r','qdatiflusso_r','mediadatiflusso_r','dsdatiflusso_r', 'mindatiflusso_r',
                 'is_sottorete']])

    df_fin =df_fin.dropna()

    data_json = json.loads(df_fin.to_json(orient='records'))

    mng_client = pymongo.MongoClient('localhost', 27017)
    mng_db = mng_client['VNCC'] # Replace mongo db name
    collection_name = 'dataset' # Replace mongo db collection name
    db_cm = mng_db[collection_name]
    db_cm.insert(data_json)

    # os.remove("cattura.js")

    print("Numero Inserimenti: ", df_fin.shape[0])