from pyspark.ml.evaluation import MulticlassClassificationEvaluator
from pyspark.ml.feature import Normalizer
from pyspark.sql import SparkSession
from pyspark.ml.linalg import Vectors
from pyspark.sql.functions import rand
from pyspark.ml.feature import StandardScaler

# SparkSession.builder.
spark = SparkSession \
    .builder \
    .appName("myApp") \
    .config("spark.mongodb.input.uri", "mongodb://localhost/VNCC.dataset") \
    .config("spark.mongodb.output.uri", "mongodb://localhost/pippo.banana") \
    .config('spark.jars.packages','org.mongodb.spark:mongo-spark-connector_2.11:2.2.0' ) \
    .getOrCreate()

sc = spark.sparkContext
df = spark.read.format("com.mongodb.spark.sql.DefaultSource").load()

'''CONVERSION AND SAVE'''
# df_pd = df.toPandas()
# df_pd.to_csv('dataset.csv')

training = (df.rdd.map(lambda x: (x[5], Vectors.dense(x[1],x[2],x[3],x[4],x[6],x[7],x[8],x[9],x[10],x[11],x[12],x[13])))).toDF()
training = training.selectExpr("_1 as label", "_2 as features")

# DATA PREPROCESSING
# Free some memory
df = None
# Shuffle dataframe
# training = training.orderBy(rand(seed=666))
#
# panda  = training.toPandas()
# panda.to_csv('panda.csv')

# Divide training from test

df_tr, df_te = training.randomSplit(weights=[0.7,0.3],seed=666)

# Normalize each Vector using $L^1$ norm.
standardizer = StandardScaler(inputCol='features',outputCol='features_scaled')
model = standardizer.fit(df_tr)
df_tr = model.transform(df_tr)
df_te = model.transform(df_te)

df_tr = df_tr[['label','features_scaled']]
df_te = df_te[['label','features_scaled']]

df_tr = df_tr.selectExpr("label as label", "features_scaled as features")
df_te = df_te.selectExpr("label as label", "features_scaled as features")

# model.save('standardizer')

# df_tr
# normalizer = Normalizer(inputCol="features", outputCol="normFeatures", p=1.0)
# l1NormData = normalizer.transform(df_tr)

#
# # LOGISTIC REGRESSION
# from pyspark.ml.classification import LogisticRegression
#
# lr = LogisticRegression()
# lrModel = lr.fit(df_tr)
#
# summary = lrModel.summary
#
# print 'Accuracy on tr:\n', summary.accuracy
# print 'Precision on tr:\n', summary.precisionByLabel
# print 'fscore on tr:\n', summary.fMeasureByLabel()
#
# predictions=lrModel.transform(df_te)
#
# def testScores(predictions):
#     acc = MulticlassClassificationEvaluator(
#         labelCol="label", predictionCol="prediction", metricName="accuracy")
#     prec = MulticlassClassificationEvaluator(
#         labelCol="label", predictionCol="prediction", metricName="weightedPrecision")
#     f1 = MulticlassClassificationEvaluator(
#         labelCol="label", predictionCol="prediction", metricName="f1")
#     accuracy = acc.evaluate(predictions)
#     precision = prec.evaluate(predictions)
#     f1_score = f1.evaluate(predictions)
#     print "Test acc = %g " % accuracy
#     print "Test Precision acc = %g " % precision
#     print "Test f1 = %g " % f1_score
#
#
# testScores(predictions)

# from pyspark.ml.classification import LinearSVC
from pyspark.ml.classification import MultilayerPerceptronClassifier
import time
layers = [12,100,3]
lr = MultilayerPerceptronClassifier(maxIter=1000, blockSize=1024,layers=layers, seed=1234)
t=time.time()
lrModelNN = lr.fit(df_tr)
print 'Elapsed training time:', time.time()-t
#summary = lrModel.summary

def testScores_NN(predictions):
    acc = MulticlassClassificationEvaluator(
        labelCol="label", predictionCol="prediction", metricName="accuracy")
    # prec = MulticlassClassificationEvaluator(
    #     labelCol="label", predictionCol="prediction", metricName="weightedPrecision")
    f1 = MulticlassClassificationEvaluator(
        labelCol="label", predictionCol="prediction", metricName="f1")
    accuracy = acc.evaluate(predictions)
    # precision = prec.evaluate(predictions)
    f1_score = f1.evaluate(predictions)
    print "acc for NN = %g " % accuracy
    # print "Test Precision acc = %g " % precision
    print "f1 for NN= %g " % f1_score

# print 'Accuracy on tr:\n', summary.accuracy
# print 'Precision on tr:\n', summary.precisionByLabel
# print 'fscore on tr:\n', summary.fMeasureByLabel()



predictions_tr=lrModelNN.transform(df_tr)

df_tr_hat_pd= predictions_tr.toPandas()
df_tr_pd = df_tr.toPandas()
df_te_hat_pd= predictions_tr.toPandas()
df_te_pd = df_te.toPandas()

# predictions_te=lrModel.transform(df_te)

print 'NN RESULTS ON TRAINING'
testScores_NN(predictions_tr)
print 'NN RESULTS ON TEST'
testScores_NN(lrModelNN.transform(df_te))

# lrModelNN.save()
