import sys, json
import time
#from bs4 import BeautifulSoup
import pandas as pd
import numpy as np
#from tqdm import tqdm
import pymongo
import os

import copy

CURRENT_LABEL = 1

#creare dataframe fatto bene
df = pd.read_csv("cattura.js", header = None)
cols = ['usec', 'sec', 'saddr', 'daddr', 'datalength']

def eliminapar (stringa):
    val = stringa.replace("{", "")
    val = stringa.replace("}", "")
    return val

def estraival (stringa):
    val = stringa.split(':')[1]
    return val

def estraiip (stringa):
    val = stringa.split('[')[1]
    vdal = val.split(']')[0]
    return val

def addLabel(i, df):
    return df.assign(label=i)


#lista = [0,1,2,3,4,13,14,15,16]
lista=[0,1,10]
for y in lista:
    df[y] = df[y].apply(estraival)

df[2] = df[2]+"."+df[3].astype(str)+"."+df[4].astype(str)+"."+df[5].astype(str)
df[6] = df[6]+"."+df[7].astype(str)+"."+df[8].astype(str)+"."+df[9].astype(str)

df = df.drop(columns = [3,4,5,7,8,9])
for y in (2,6):
    df[y] = df[y].astype(str).apply(estraiip)

df[10] = df[10].apply(eliminapar)
df.columns = cols

#raggruppare i dati
#listaip = df.saddr.unique()
df['totusec'] = df['sec'].astype(np.int64).apply(lambda x: x*1000000) + (df['usec'].astype(np.int64))
df['datalength'] = df['datalength'].astype(int)

def flusso_pacchetti (x, df_in):
    send_feats = estrai_pacchetti(df_in, x['saddr'], x['daddr'], x.totusec)
    rec_feats = estrai_pacchetti (df_in, x['daddr'], x['saddr'], x.totusec)
    #t.update(1)
    is_sottorete = 0
    if (x.saddr.startswith('10.0.0.')):
        is_sottorete = 1

    tot_feats = send_feats+rec_feats
    tot_feats.append(is_sottorete)
    #print(len(tot_feats))
#    tot_feats.extend(send_feats)
#    tot_feats.extend(rec_feats)
#    tot_feats.append(is_sottorete)
    return tot_feats

def estrai_pacchetti (df, s_ip, d_ip, temp):
    df_new = df.loc[(df['saddr'] == s_ip) & (df['daddr'] == d_ip)  & (df['totusec'] < temp)]
    df_new = df_new.loc[(temp - df_new['totusec'] < 30000)]
    return  [df_new.shape[0], sum(df_new['datalength']), df_new['datalength'].mean(), df_new['datalength'].std(), df_new.min()['datalength']]

df_in = df[['saddr', 'daddr', 'datalength', 'totusec']]
#df_in = df_in[0:10]

#t = tqdm(total = df_in.shape[0])

df[['ndatiflusso_s','qdatiflusso_s','mediadatiflusso_s','dsdatiflusso_s', 'mindatiflusso_s',
    'ndatiflusso_r','qdatiflusso_r','mediadatiflusso_r','dsdatiflusso_r', 'mindatiflusso_r',
    'is_sottorete']] = pd.DataFrame(list(df_in.apply(lambda x: flusso_pacchetti(x, df_in), axis=1)))

# t.close()
#
# df_fin = df[['datalength','ndatiflusso_s','qdatiflusso_s','mediadatiflusso_s','dsdatiflusso_s', 'mindatiflusso_s',
#              'ndatiflusso_r','qdatiflusso_r','mediadatiflusso_r','dsdatiflusso_r', 'mindatiflusso_r',
#              'is_sottorete']]
df_fin = addLabel(CURRENT_LABEL,df[['datalength','ndatiflusso_s','qdatiflusso_s','mediadatiflusso_s','dsdatiflusso_s', 'mindatiflusso_s',
             'ndatiflusso_r','qdatiflusso_r','mediadatiflusso_r','dsdatiflusso_r', 'mindatiflusso_r',
             'is_sottorete']])

df_fin =df_fin.dropna()
#jsonDf = df_fin.to_json(orient='records')

data_json = json.loads(df_fin.to_json(orient='records'))

mng_client = pymongo.MongoClient('localhost', 27017)
mng_db = mng_client['VNCC'] # Replace mongo db name
collection_name = 'dataset' # Replace mongo db collection name
db_cm = mng_db[collection_name]
db_cm.insert(data_json)

os.remove("cattura.js")

print("Numero Inserimenti: ", df_fin.shape[0])